# Rover
We are on a mission to control a rover that explores Mars planet. And the mission control already gave us rover instructions to move into specific directions and positions.

## Dependencies
``Pipfile``

    [[source]]
    url = "https://pypi.org/simple"
    verify_ssl = true
    name = "pypi"

    [packages]
    django = "~=4.1"
    environ = "*"
    gunicorn = "*"
    django-environ = "*"
    requests = "*"
    django-extensions = "*"
    djangorestframework = "*"
    nested-lookup = "*"
    freezegun = "*"
    ipython = "~=8.4.0"

    [requires]
    python_version = "3.8.13"

## Development
### Running locally
Preparation

    $ cp .env.example .env

    $ docker build -t rover .

Running API

    $ docker run -it --env-file='.env' -p 8001:8000 rover:latest /bin/bash

    $ ./manage.py runserver 0.0.0.0:8000
---
## Create Map
#### host : http://localhost:8001/api/v1/map
#### method : post
#### payload : 
```
{
	"map_size": 10
}
```
#### result:
```
{
	"output": "N:0,0"
}
```

## Control the Rover
#### host : http://localhost:8001/api/v1/rover
#### method : post
#### pyload : 
```
{
	"action": "F"
}
```
#### result :
```
{
	"output": "N:0,1"
}
```

### Check the current position of the rover
#### host : http://localhost:8001/api/v1/rover
#### method : get
#### result : 
```
{
	"id": 16,
	"map_id": 15,
	"user": "System",
	"action": "F",
	"direction": "N",
	"position_y": 1,
	"position_x": 0,
	"timestamp": "2022-08-27T07:34:46.017053Z"
}
```
---
## Rover controller

| Action in pyload | Rover action  |
| ------- | --- |
| F | moving forward 1 block |
| F2 | moving forward 2 block |
| B | moving backward 1 block |
| B2 | moving backward 2 block |
| L | turn left |
| R | turn right |
| HL | turn half left |
| HR | turn half right |

---
**Warning**
    
The robot cannot move outside the map.
for example,
```
{
	"action": "F11"
}
```
or 
```
{
	"action": "B11"
}
```
```
"Cannot move to that positon."
```