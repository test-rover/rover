from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from processor.processor import TransactionProcessor, MapProcessor


class MapApiView(APIView):

    def get_user(self):
        return 'System'

    def get(self, *args, **kwargs):
        try:
            r = MapProcessor(self.get_user()).get()
        except Exception as e:
            r = str(e)
            s = status.HTTP_404_NOT_FOUND
        else:
            s = status.HTTP_201_CREATED
        finally:
            return Response(r, status=s)

    def post(self, request, *args, **kwargs):
        try:
            r = MapProcessor(self.get_user()).process(request.data)
        except Exception as e:
            r = str(e)
            s = status.HTTP_400_BAD_REQUEST
        else:
            s = status.HTTP_201_CREATED
        finally:
            return Response(r, status=s)


class RoverApiView(APIView):

    def get_user(self):
        return 'System'

    def get(self, request, *args, **kwargs):
        try:
            r = TransactionProcessor(self.get_user()).get()
        except Exception as e:
            r = str(e)
            s = status.HTTP_404_NOT_FOUND
        else:
            s = status.HTTP_201_CREATED
        finally:
            return Response(r, status=s)


    def post(self, request, *args, **kwargs):
        try:
            r = TransactionProcessor(self.get_user()).process(request.data)
        except Exception as e:
            r = str(e)
            s = status.HTTP_400_BAD_REQUEST
        else:
            s = status.HTTP_201_CREATED
        finally:
            return Response(r, status=s)
