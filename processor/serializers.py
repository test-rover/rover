from rest_framework import serializers
from processor.models import Map, Transaction


class MapSerializer(serializers.ModelSerializer):

    class Meta:
        model = Map
        fields = '__all__'


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = '__all__'
