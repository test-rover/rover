from processor.validator import ActionValidator, PositionValidator
from nested_lookup import nested_lookup
from processor.factory import (
    MapFactory, TransactionFactory, CalculatorDirectionFactory,
    CalculatorPositionFactory
)
from processor.builder import TransactionBuilder
from django.utils import timezone
from django.conf import settings


class MapProcessor:

    def __init__(self, user):
        self.map_factory = MapFactory()
        self.transaction_factory = TransactionFactory()
        self.user = user

    def get(self):
        return self.map_factory.get()

    def create_transaction(self, map_id):
        self.transaction_factory.create({
            "map_id": map_id,
            "user": self.user,
            "action": "-",
            "direction": "N",
            "position_y": 0,
            "position_x": 0,
            "timestamp": timezone.now()
        })

    def create(self, data):
        self.map_factory.delete()
        self.transaction_factory.delete()
        return self.map_factory.create(data)

    def enrich(self, data):
        data['timestamp'] = timezone.now()

    def process(self, data):
        self.enrich(data)
        id = self.create(data)
        self.create_transaction(id)
        return TransactionBuilder().response('N', 0, 0)


class TransactionProcessor:

    def __init__(self, user):
        self.m = MapFactory()
        self.t = TransactionFactory()
        self.cd = CalculatorDirectionFactory()
        self.cp = CalculatorPositionFactory()
        self.user = user

    def get(self):
        return self.t.get()

    def list(self):
        return self.t.list()

    def get_map_size(self):
        return self.m.get().get('map_size')

    def filters(self, lookup_field, source):
        if (lookup := nested_lookup(lookup_field, source)):
            return lookup[0]
        return None

    def create(self, map_id, action, d, x, y):
        cp = self.cp.calculate(action, d, x, y)
        self.t.create({
            "map_id": map_id,
            "user": self.user,
            "action": action,
            "direction": self.cd.calculate(d, action),
            "position_y": cp[1],
            "position_x": cp[0],
            "timestamp": timezone.now()
        })

    def response(self):
        t = self.t.get()
        return TransactionBuilder().response(
            t['direction'], t['position_y'], t['position_x']
        )

    def process(self, data):
        t = self.get()
        action = self.filters('action', data)
        ActionValidator().validate(action)
        d = self.filters('direction', t)
        x = self.filters('position_x', t)
        y = self.filters('position_y', t)
        m = self.get_map_size()
        if action[0] in settings.MOVEMENT:
            PositionValidator().validate(action, d, x, y, m)
        self.create(self.filters('map_id', t), action, d, x, y)
        return self.response()
