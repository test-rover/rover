from django.urls import path
from processor.views import RoverApiView, MapApiView


urlpatterns = [
    path('map', MapApiView.as_view(), name='map'),
    path('rover', RoverApiView.as_view(), name='rover')
]
