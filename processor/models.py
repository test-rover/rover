from django.db import models


class Map(models.Model):
    map_size = models.PositiveIntegerField()
    timestamp = models.DateTimeField()


class Transaction(models.Model):
    map_id = models.IntegerField()
    user = models.CharField(max_length=100)
    action = models.CharField(max_length=10)
    direction = models.CharField(max_length=2)
    position_y = models.PositiveIntegerField()
    position_x = models.PositiveIntegerField()
    timestamp = models.DateTimeField()
