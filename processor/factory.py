from processor.serializers import MapSerializer, TransactionSerializer
from processor.models import Map, Transaction
from django.conf import settings
from copy import deepcopy


class FactoryBase:

    serialzer = None
    model = None

    def create(self, data):
        s = self.serialzer(data=data)
        s.is_valid(raise_exception=True)
        obj = s.save()
        return obj.id

    def delete(self):
        data = self.model.objects.all()
        data.delete()

    def get(self):
        m = self.model.objects.last()
        return self.serialzer(m).data

    def list(self):
        m = self.model.objects.all()
        return self.serialzer(m).data


class MapFactory(FactoryBase):
    serialzer = MapSerializer
    model = Map


class TransactionFactory(FactoryBase):
    serialzer = TransactionSerializer
    model = Transaction


class CalculatorDirectionFactory:

    def __init__(self):
        self.d = deepcopy(settings.DIRECTION)
        self.rd = deepcopy(settings.DIRECTION)
        self.rd.reverse()

    def calculate(self, d, action):
        if action[0] in settings.MOVEMENT:
            return d
        if action == 'R':
            index = self.d.index(d) + 2
            return self.d[index if index < 7 else index - 8]
        if action == 'HR':
            index = self.d.index(d) + 1
            return self.d[index if index < 7 else index - 8]

        if action == 'L':
            index = self.rd.index(d) + 2
            return self.rd[index if index < 7 else index - 8]
        if action == 'HL':
            index = self.rd.index(d) + 1
            return self.rd[index if index < 7 else index - 8]


class CalculatorPositionFactory:

    def get_sub(self, action):
        return action.split(action[0], 1)[1]

    def calculate_forward(self, direction, x, y, moving):
        mapping = {
            'N': [x, y + moving],
            'E': [x + moving, y],
            'S': [x, y - moving],
            'W': [x - moving, y],
            'NE': [x + moving, y + moving],
            'NW': [x - moving, y + moving],
            'SW': [x - moving, y - moving],
            'SE': [x + moving, y - moving]
        }
        return mapping[direction]

    def calculate_backward(self, direction, x, y, moving):
        mapping = {
            'N': [x, y - moving],
            'E': [x - moving, y],
            'S': [x, y + moving],
            'W': [x + moving, y],
            'NE': [x - moving, y - moving],
            'NW': [x + moving, y - moving],
            'SW': [x + moving, y + moving],
            'SE': [x - moving, y + moving]
        }
        return mapping[direction]

    def calculate(self, action, direction, x, y):
        if action[0] in settings.ACTION:
            return [x, y]
        moving = 1 if len(action) == 1 else self.get_sub(action)
        if action[0] == 'F':
            return self.calculate_forward(
                direction, int(x), int(y), int(moving)
            )
        return self.calculate_backward(direction, int(x), int(y), int(moving))
