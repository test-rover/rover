from django.test import TestCase
from processor.processor import MapProcessor, TransactionProcessor
from django.utils import timezone
from freezegun import freeze_time


class MapProcessorTestCase(TestCase):

    def setUp(self):
        self.p = MapProcessor('System')

    def test_get_not_map(self):
        r = self.p.get()
        self.assertDictEqual(r, {'map_size': None, 'timestamp': None})

    @freeze_time("1984-08-15")
    def test_create(self):
        data = {"map_size": 10, "timestamp": timezone.now()}
        self.assertEqual(self.p.create(data), 1)

    @freeze_time("1984-08-15")
    def test_get_last_map(self):
        data = {"map_size": 10, "timestamp": timezone.now()}
        self.assertEqual(self.p.create(data), 1)
        r = self.p.get()
        self.assertDictEqual(
            r, {'id': 1, 'map_size': 10, 'timestamp': '1984-08-15T00:00:00Z'}
        )

    @freeze_time("1984-08-15")
    def test_process(self):
        data = {"map_size": 10}
        r = self.p.process(data)
        self.assertDictEqual(r, {'output': 'N:0,0'})


class TransactionProcessorTestCase(TestCase):

    def setUp(self):
        self.t = TransactionProcessor('System')

    @freeze_time("1984-08-15")
    def test_get(self):
        MapProcessor('System').process({"map_size": 10})
        r = self.t.get()
        self.assertDictEqual(
            {
                'id': 1,
                'map_id': 1,
                'user': 'System',
                'action': '-',
                'direction': 'N',
                'position_y': 0,
                'position_x': 0,
                'timestamp': '1984-08-15T00:00:00Z'
            }, r
        )

    def test_turn_right(self):
        MapProcessor('System').process({"map_size": 10})
        r = self.t.process({'action': 'R'})
        self.assertDictEqual(r, {'output': 'E:0,0'})

    def test_forward_from_e(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        r = self.t.process({'action': 'F'})
        self.assertDictEqual(r, {'output': 'E:1,0'})

    def test_turn_left(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        r = self.t.process({'action': 'L'})
        self.assertDictEqual(r, {'output': 'N:1,0'})

    def test_forward_from_n(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'N:1,0'}
        )
        r = self.t.process({'action': 'F'})
        self.assertDictEqual(r, {'output': 'N:1,1'})

    def test_turn_left_from_n(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'N:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'N:1,1'}
        )
        r = self.t.process({'action': 'L'})
        self.assertDictEqual(r, {'output': 'W:1,1'})

    def test_turn_left_from_w(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'N:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'N:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'W:1,1'}
        )
        r = self.t.process({'action': 'L'})
        self.assertDictEqual(r, {'output': 'S:1,1'})

    def test_forward_from_s(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'N:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'N:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'W:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'S:1,1'}
        )
        r = self.t.process({'action': 'F'})
        self.assertDictEqual(r, {'output': 'S:1,0'})

    def test_turn_right_from_s(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'R'}), {'output': 'E:0,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'E:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'N:1,0'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'N:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'W:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'L'}), {'output': 'S:1,1'}
        )
        self.assertDictEqual(
            self.t.process({'action': 'F'}), {'output': 'S:1,0'}
        )
        r = self.t.process({'action': 'R'})
        self.assertDictEqual(r, {'output': 'W:1,0'})

    def test_turn_right_half(self):
        MapProcessor('System').process({"map_size": 10})
        r = self.t.process({'action': 'HR'})
        self.assertDictEqual(r, {'output': 'NE:0,0'})

    def test_forward_from_ne(self):
        MapProcessor('System').process({"map_size": 10})
        self.assertDictEqual(
            self.t.process({'action': 'HR'}), {'output': 'NE:0,0'}
        )
        r = self.t.process({'action': 'F'})
        self.assertDictEqual(r, {'output': 'NE:1,1'})

    def test_forward_from_n_with_f2(self):
        MapProcessor('System').process({"map_size": 10})
        r = self.t.process({'action': 'F2'})
        self.assertDictEqual(r, {'output': 'N:0,2'})
