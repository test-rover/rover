from django.test import TestCase
from processor.validator import ActionValidator, PositionValidator


class ActionValidatorTestCase(TestCase):

    def test_action_forward(self):
        self.assertTrue(ActionValidator().validate('F'))

    def test_action_forward_move_1(self):
        self.assertTrue(ActionValidator().validate('F1'))

    def test_action_forward_with_sub_not_int(self):
        with self.assertRaises(Exception) as e:
            ActionValidator().validate('FF')
        self.assertEqual(str(e.exception), 'Action FF not allowed.')

    def test_action_backward(self):
        self.assertTrue(ActionValidator().validate('B'))

    def test_action_backward_move_1(self):
        self.assertTrue(ActionValidator().validate('B1'))

    def test_action_backward_with_sub_not_int(self):
        with self.assertRaises(Exception) as e:
            ActionValidator().validate('BF')
        self.assertEqual(str(e.exception), 'Action BF not allowed.')

    def test_action_move_left(self):
        self.assertTrue(ActionValidator().validate('L'))

    def test_action_move_right(self):
        self.assertTrue(ActionValidator().validate('R'))

    def test_action_move_half(self):
        with self.assertRaises(Exception) as e:
            ActionValidator().validate('H')
        self.assertEqual(str(e.exception), 'Action H not allowed.')

    def test_action_move_half_left(self):
        self.assertTrue(ActionValidator().validate('HL'))

    def test_action_move_half_right(self):
        self.assertTrue(ActionValidator().validate('HR'))

    def test_action_move_left_1(self):
        with self.assertRaises(Exception) as e:
            ActionValidator().validate('L1')
        self.assertEqual(str(e.exception), 'Action L1 not allowed.')


class PositionValidatorTestCase(TestCase):

    def test_f_n_0_0_3(self):
        self.assertTrue(PositionValidator().validate('F', 'N', 0, 0, 3))

    def test_f3_n_0_0_3(self):
        self.assertTrue(PositionValidator().validate('F3', 'N', 0, 0, 3))

    def test_f4_n_0_0_3_over_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('F4', 'N', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_f_e_0_0_3(self):
        self.assertTrue(PositionValidator().validate('F', 'E', 0, 0, 3))

    def test_f_s_0_0_3_under_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('F', 'S', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_f_w_0_0_3_under_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('F', 'W', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_b_n_0_0_3_under_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('B', 'N', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_b_e_0_0_3_under_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('B', 'E', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_b_s_0_0_3(self):
        self.assertTrue(PositionValidator().validate('B', 'S', 0, 0, 3))

    def test_b_w_0_0_3(self):
        self.assertTrue(PositionValidator().validate('B', 'W', 0, 0, 3))

    def test_f_n_0_3_3_over_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('F', 'N', 0, 3, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_f_ne_0_0_3(self):
        self.assertTrue(PositionValidator().validate('F', 'NE', 0, 0, 3))

    def test_b_ne_0_0_3_under_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('B', 'NE', 0, 0, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')

    def test_f3_ne_0_0_3(self):
        self.assertTrue(PositionValidator().validate('F3', 'NE', 0, 0, 3))

    def test_f3_ne_1_1_3_over_map(self):
        with self.assertRaises(Exception) as e:
            PositionValidator().validate('F3', 'NE', 1, 1, 3)
        self.assertEqual(str(e.exception), 'Cannot move to that positon.')
