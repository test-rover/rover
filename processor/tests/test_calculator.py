from django.test import TestCase
from processor.factory import CalculatorDirectionFactory


class CalculatorDirectionFactoryTestCase(TestCase):
    
    def setUp(self):
        self.c = CalculatorDirectionFactory()

    def test_turn_right_from_nort(self):
        r = self.c.calculate('N', 'R')
        self.assertEqual(r, 'E')

    def test_turn_right_from_east(self):
        r = self.c.calculate('E', 'R')
        self.assertEqual(r, 'S')

    def test_turn_right_from_south(self):
        r = self.c.calculate('S', 'R')
        self.assertEqual(r, 'W')

    def test_turn_right_from_west(self):
        r = self.c.calculate('W', 'R')
        self.assertEqual(r, 'N')

    def test_turn_right_half_from_nort(self):
        r = self.c.calculate('N', 'HR')
        self.assertEqual(r, 'NE')

    def test_turn_right_half_from_east(self):
        r = self.c.calculate('E', 'HR')
        self.assertEqual(r, 'SE')

    def test_turn_right_half_from_south(self):
        r = self.c.calculate('S', 'HR')
        self.assertEqual(r, 'SW')

    def test_turn_right_half_from_west(self):
        r = self.c.calculate('W', 'HR')
        self.assertEqual(r, 'NW')

    def test_turn_left_from_nort(self):
        r = self.c.calculate('N', 'L')
        self.assertEqual(r, 'W')

    def test_turn_left_from_west(self):
        r = self.c.calculate('W', 'L')
        self.assertEqual(r, 'S')

    def test_turn_left_from_south(self):
        r = self.c.calculate('S', 'L')
        self.assertEqual(r, 'E')

    def test_turn_left_from_east(self):
        r = self.c.calculate('E', 'L')
        self.assertEqual(r, 'N')
