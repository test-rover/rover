from django.conf import settings


class ActionValidator:

    def get_sub(self, action):
        return action.split(action[0], 1)[1]

    def is_integer(self, sub, action):
        try:
            int(sub)
            return True
        except ValueError:
            raise Exception(f"Action {action} not allowed.")

    def validate(self, action):
        if action[0] not in settings.ACTION + settings.MOVEMENT:
            raise Exception(f"Action {action} not allowed.")
        if len(action) > 1:
            s = self.get_sub(action)
            if action[0] in settings.MOVEMENT:
                if self.is_integer(s, action):
                    return True
            else:
                if s not in ['L', 'R']:
                    raise Exception(f"Action {action} not allowed.")
        else:
            if action[0] == "H":
                raise Exception(f"Action {action} not allowed.")
        return True


class PositionValidator(ActionValidator):

    def forward_validation(self, direction, x, y, moviing, map):
        if direction in ['N', 'NE', 'NW'] and (y + moviing) > map:
            raise Exception(f"Cannot move to that positon.")
        if direction == 'E' and (x + moviing) > map:
            raise Exception(f"Cannot move to that positon.")
        if direction in ['S', 'SE', 'SW'] and (y - moviing) < 0:
            raise Exception(f"Cannot move to that positon.")
        if direction == 'W' and (x - moviing) < 0:
            raise Exception(f"Cannot move to that positon.")

    def backward_validation(self, direction, x, y, moviing, map):
        if direction in ['N', 'NE', 'NW'] and (y - moviing) < 0:
            raise Exception(f"Cannot move to that positon.")
        if direction == 'E' and (x - moviing) < 0:
            raise Exception(f"Cannot move to that positon.")
        if direction in ['S', 'SE', 'SW'] and (y + moviing) > map:
            raise Exception(f"Cannot move to that positon.")
        if direction == 'W' and (x + moviing) > map:
            raise Exception(f"Cannot move to that positon.")

    def validate(self, action, direction, x, y, map):
        moving = 1 if len(action) == 1 else self.get_sub(action)
        if action[0] == 'F':
            self.forward_validation(
                direction, int(x), int(y), int(moving), map
            )
        else:
            self.backward_validation(
                direction, int(x), int(y), int(moving), map
            )
        return True
